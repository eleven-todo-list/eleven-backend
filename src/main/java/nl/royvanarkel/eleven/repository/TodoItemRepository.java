package nl.royvanarkel.eleven.repository;

import nl.royvanarkel.eleven.domain.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoItemRepository extends JpaRepository<TodoItem, Long> {
}
