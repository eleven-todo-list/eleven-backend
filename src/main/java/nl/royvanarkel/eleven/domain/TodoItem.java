package nl.royvanarkel.eleven.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "todoItems")
public class TodoItem {

    private long id;
    private String name;
    private boolean completed;
    private Date dueDate;

    public TodoItem() {
        super();
    }

    public TodoItem(String name, boolean completed, Date dueDate) {
        super();
        this.name = name;
        this.completed = completed;
        this.dueDate = dueDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "status")
    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Column(name = "due_date")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
}
