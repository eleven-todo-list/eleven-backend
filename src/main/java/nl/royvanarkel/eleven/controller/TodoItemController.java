package nl.royvanarkel.eleven.controller;

import nl.royvanarkel.eleven.domain.TodoItem;
import nl.royvanarkel.eleven.exception.ResourceNotFoundException;
import nl.royvanarkel.eleven.repository.TodoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class TodoItemController {

    @Autowired
    private TodoItemRepository todoItemRepository;

    // create
    @PostMapping("/todoitems")
    public TodoItem createTodoItem(@Valid @RequestBody TodoItem todoItem) {
        return todoItemRepository.save(todoItem);
    }

    // read
    @GetMapping("/todoitems")
    public List<TodoItem> getAllTodoItems() {
       return todoItemRepository.findAll();
    }

    // read by id
    @GetMapping("/todoitems/{id}")
    public ResponseEntity<?> getTodoItemById(@PathVariable long id) throws ResourceNotFoundException {
        TodoItem todoItem = todoItemRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TodoItem not found with id " + id ));
        return ResponseEntity.ok().body(todoItem);

    }

    // update
    @PutMapping("/todoitems/{id}")
    public ResponseEntity<?> updateTodoItemById(@RequestBody TodoItem todoItemDetails, @PathVariable long id) throws ResourceNotFoundException {
        TodoItem todoItem = todoItemRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TodoItem not found with id " + id ));

        todoItem.setName(todoItemDetails.getName());
        todoItem.setDueDate(todoItemDetails.getDueDate());
        todoItem.setCompleted(todoItemDetails.isCompleted());

        return ResponseEntity.ok(todoItemRepository.save(todoItem));
    }

    // delete
    @DeleteMapping("/todoitems/{id}")
    public void deleteTodoItem(@PathVariable long id) {
        todoItemRepository.deleteById(id);
    }
}
